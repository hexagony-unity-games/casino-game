﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.Model
{
    internal interface ISerializable
    {
        public void Serialize(Stream s);
        public void Deserialize(Stream s);
    }
}
