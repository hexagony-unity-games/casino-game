﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.Model
{
    internal class AppSettings
    {
        private static int s_versionCode = 1;

        public int CurrentUserId { get; set; }

        public bool IsAnyUserRegistered { get; set; }

        public AppSettings() : this(-1, false)
        {
        }

        public AppSettings(int currentUserId, bool isAnyUserRegistered)
        {
            CurrentUserId = currentUserId;
            IsAnyUserRegistered = isAnyUserRegistered;
        }

        public void Serialize(Stream s)
        {
            using (var bw = new BinaryWriter(s, Encoding.Default, true))
            {
                bw.Write(s_versionCode);
                bw.Write(CurrentUserId);
                bw.Write(IsAnyUserRegistered);
            }
        }

        public void Deserialize(Stream s)
        {
            using (var br = new BinaryReader(s, Encoding.Default, true))
            {
                int curVc = br.ReadInt32();

                if (curVc > 0)
                {
                    CurrentUserId = br.ReadInt32();
                    IsAnyUserRegistered = br.ReadBoolean();
                }
            }
        }
    }
}
