﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Profiling;

namespace Scripts.Model
{
    internal class UserProfile : ISerializable
    {
        private static readonly int s_versionCode = 1;
        private static int s_lastValidId = 1;

        private int _coins;

        private List<GameRoundResult> _allPlayedGames = new();

        public int Coins { 
            get => _coins; 
            set => _coins = value >= 0 ? value : 0; 
        }

        public string Name { get; private set; }

        public int Id { get; private set; }

        public float AvgWinningScore
        {
            get
            {
                int totalWinAmount = _allPlayedGames
                    .Aggregate(0, (acc, p) => acc + p.WinningAmount);

                int totalWinGames = _allPlayedGames
                    .Aggregate(0, (acc, p) => acc + Convert.ToInt32(p.IsWinningRound));

                return (float)totalWinAmount / totalWinGames;
            }
        }

        public float WinningPercent
        {
            get
            {
                int totalWinGames = _allPlayedGames
                    .Aggregate(0, (acc, p) => acc + Convert.ToInt32(p.IsWinningRound));

                return (float)totalWinGames / _allPlayedGames.Count;
            }
        }

        public UserProfile() : this("", 0)
        {
            Id = s_lastValidId++;
        }

        public UserProfile(string name)
        {
            Name = name;
        }

        public UserProfile(string name, int coins) : this(name)
        {
            _coins = coins;
        }

        public void AddPlayedGame(GameRoundResult result)
        {
            _allPlayedGames.Add(result);
        }

        public void Serialize(Stream s)
        {
            using (var bw = new BinaryWriter(s, Encoding.Default, true))
            {
                bw.Write(s_versionCode);
                bw.Write(Id);
                bw.Write(Coins);
                bw.Write(Name);
                
                bw.Write(_allPlayedGames.Count);
            }

            _allPlayedGames.ForEach(g => g.Serialize(s));
        }

        public void Deserialize(Stream s)
        {
            using (var br = new BinaryReader(s, Encoding.Default, true))
            {
                int curVc = br.ReadInt32();

                if (curVc > 0)
                {
                    Id = br.ReadInt32();
                    s_lastValidId = Math.Max(s_lastValidId, Id + 1);

                    Coins = br.ReadInt32();
                    Name = br.ReadString();

                    int gamesCount = br.ReadInt32();

                    for (int i = 0; i < gamesCount; i++)
                    {
                        GameRoundResult result = new();
                        result.Deserialize(s);
                        _allPlayedGames.Add(result);
                    }
                }
            }
        }
    }
}
