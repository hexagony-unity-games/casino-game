﻿using System;
using System.IO;
using System.Text;

namespace Scripts.Model
{
    internal record GameRoundResult : ISerializable
    {
        private static readonly int s_versionCode = 1;

        public int WinningAmount { get; private set; }

        public bool IsWinningRound { get; private set; }

        public GameRoundResult()
        {
            WinningAmount = 0;
            IsWinningRound = false;
        }

        public GameRoundResult(bool isWinningRound, int winningAmount) 
        {
            WinningAmount = winningAmount;
            IsWinningRound = isWinningRound;
        }
        public void Serialize(Stream s)
        {
            using (var bw = new BinaryWriter(s, Encoding.Default, true))
            {
                bw.Write(s_versionCode);
                bw.Write(WinningAmount);
                bw.Write(IsWinningRound);
            }
        }

        public void Deserialize(Stream s)
        {
            using (var br = new BinaryReader(s, Encoding.Default, true))
            {
                int curVc = br.ReadInt32();

                if (curVc > 0)
                {
                    WinningAmount = br.ReadInt32();
                    IsWinningRound = br.ReadBoolean();
                }
            }
        }

        public override string ToString()
        {
            return $"{{win:{IsWinningRound}, coins:{WinningAmount}}}";
        }
    }
}
