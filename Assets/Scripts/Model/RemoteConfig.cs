﻿using Scripts.Api.Const;

namespace Scripts.Model
{
    internal class RemoteConfig
    {
        public bool WebViewEnabled { get; } = false;

        public string WebViewUrl { get; } = "";

        public RemoteConfig(bool webViewEnabled, string webViewUrl)
        {
            WebViewEnabled = webViewEnabled;
            WebViewUrl = webViewUrl;
        }

        public override string ToString()
        {
            return $"{{" +
                $"{RemoteConfigKey.WebViewEnabled}:{WebViewEnabled}, " +
                $"{RemoteConfigKey.WebViewUrl}:{WebViewUrl}" +
                $"}}";
        }
    }
}
