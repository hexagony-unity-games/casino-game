﻿using Scripts.Repository;
using Scripts.Repository.Interface;
using Scripts.ViewModel;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts
{
    internal class App : MonoBehaviour
    {
        [Header("Web")]
        [SerializeField] private RemoteConfigViewModel _remoteConfigVm;
        [SerializeField] private WebViewerViewModel _webViewerWm;
        [SerializeField] private CloudMessagingViewModel _cloudMessagingWm;
        [Header("Data")]
        [SerializeField] private AppSettingsViewModel _appSettingsVm;
        [SerializeField] private UserProfilesViewModel _userProfilesVm;
        [Header("Screens")]
        [SerializeField] private RegisterScreenViewModel _registerScreenVm;
        [SerializeField] private LoadingScreenViewModel _loadingScreenWm;
        [SerializeField] private GameScreenViewModel _gameScreenWm;
        [SerializeField] private UserProfileScreenVM _userProfileScreenVm;

        private App _instance;

        private void Start()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this);
            } 
            else
            {
                Destroy(gameObject);
            }
            
            if (_instance == this)
            {
                _remoteConfigVm.Init();
                _cloudMessagingWm.Init();
                _appSettingsVm.Init();
                _userProfilesVm.SetDependencies(new Dictionary<string, AViewModel>
                    {
                        {typeof(AppSettingsViewModel).Name, _appSettingsVm},
                    });
                _userProfilesVm.Init();

                _webViewerWm.Init();

                _gameScreenWm.SetDependencies(new Dictionary<string, AViewModel>
                    {
                        {typeof(UserProfilesViewModel).Name, _userProfilesVm},
                        {typeof(UserProfileScreenVM).Name, _userProfileScreenVm},
                    });
                _gameScreenWm.Init();

                _registerScreenVm.SetDependencies(new Dictionary<string, AViewModel>
                    {
                        {typeof(UserProfilesViewModel).Name, _userProfilesVm},
                        {typeof(GameScreenViewModel).Name, _gameScreenWm},
                    });
                _registerScreenVm.Init();

                _userProfileScreenVm.SetDependencies(new Dictionary<string, AViewModel>
                    {
                        {typeof(UserProfilesViewModel).Name, _userProfilesVm},
                        {typeof(GameScreenViewModel).Name, _gameScreenWm},
                    });
                _userProfileScreenVm.Init();

                _loadingScreenWm
                    .SetDependencies(new Dictionary<string, AViewModel>
                    {
                        {typeof(WebViewerViewModel).Name, _webViewerWm},
                        {typeof(RemoteConfigViewModel).Name, _remoteConfigVm},
                        {typeof(GameScreenViewModel).Name, _gameScreenWm},
                        {typeof(RegisterScreenViewModel).Name, _registerScreenVm},
                        {typeof(AppSettingsViewModel).Name, _appSettingsVm},
                    });
                _loadingScreenWm.Init();
            }
        }
    }
}
