﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Scripts.View.SlotsMachine
{
    internal class SlotsMachine : MonoBehaviour
    {
        [SerializeField] private List<ItemsLine> _itemsLines;
        [SerializeField] private AnimationCurve _slowDownCurve = AnimationCurve.EaseInOut(0, 1, 1, 0);

        [SerializeField] private float _slowDownSpeedMul = 0.1f;

        [SerializeField] private float _endSpeed = 1.0f;
        [SerializeField] private float _startSpeedMax = 30f;
        [SerializeField] private float _startSpeedMin = 25f;

        private UnityEvent _onRotationStarted = new();
        private UnityEvent<SlotItem[,]> _onRotationEnded = new();

        public UnityEvent OnRotationStarted => _onRotationStarted;
        public UnityEvent<SlotItem[,]> OnRotationEnded => _onRotationEnded;

        public void InitMachine()
        {
            _itemsLines.ForEach(i => i.InitLine());
        }

        public void StartRotation()
        {
            StartCoroutine(RotationRoutine());
        }

        public SlotItem[,] GetVisibleItemsMatrix()
        {
            var matrix = new SlotItem[_itemsLines[0].VisibleItemsCount, _itemsLines.Count];

            for (int x = 0; x < _itemsLines.Count; ++x)
            {
                int offset = (_itemsLines[x].ItemsCount - _itemsLines[x].VisibleItemsCount) / 2;

                for (int y = 0; y < _itemsLines[x].VisibleItemsCount; ++y)
                {
                    matrix[y, x] = _itemsLines[x].Items[offset + y];
                }
            }

            return matrix;
        }

        private IEnumerator RotationRoutine()
        {
            _onRotationStarted?.Invoke();

            yield return StartCoroutine(StartRotationRoutine());
            yield return StartCoroutine(SlowDownRotationRoutine());
            yield return StartCoroutine(StopRotationRoutine());

            _onRotationEnded?.Invoke(GetVisibleItemsMatrix());
        }

        private IEnumerator StartRotationRoutine()
        {
            foreach (var itemsLine in _itemsLines)
            {
                itemsLine.SpeedMultiplier = Random.Range(_startSpeedMin, _startSpeedMax);
                itemsLine.StartRotation();
                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator SlowDownRotationRoutine()
        {
            List<float> _startRotationSpeeds = _itemsLines
                .Select(i => i.SpeedMultiplier)
                .ToList();

            List<float> _speedsDistances = _startRotationSpeeds
                .Select(s => _endSpeed - s)
                .ToList();

            float timer = 0f;
            float maxTime = 1f;

            while (timer <= maxTime)
            {
                timer += Time.deltaTime * _slowDownSpeedMul;

                for (int i = 0; i < _itemsLines.Count; i++)
                {
                    float newSpeed = 
                        _startRotationSpeeds[i] + 
                        _slowDownCurve.Evaluate(timer) * _speedsDistances[i];

                    _itemsLines[i].SpeedMultiplier = newSpeed;
                }

                yield return null;
            }
        }

        private IEnumerator StopRotationRoutine()
        {
            List<Coroutine> _routines = _itemsLines
                .Select(i => StartCoroutine(i.StopRotationRoutine()))
                .ToList();

            foreach(var routine in _routines)
            {
                yield return routine;
            }
        }
    }
}
