﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scripts.View.SlotsMachine
{
    internal class ItemsLine : MonoBehaviour
    {
        [SerializeField] private GameObject _content;
        [SerializeField] private int _visibleItemsCount = 5;
        [SerializeField] private int _itemsSpacing = 16;
        [SerializeField] private int _lineWidth = 200;
        [SerializeField] private SlotItem _centerItem;
        [SerializeField] private Vector2 _itemSize = new(160, 160);

        private Coroutine _rotateRoutine;

        private List<SlotItem> _items = new();
        private int _centerItemIndex = 0;

        private bool _isRotateInProgress = false;
        private bool _isRotateFinished = false;

        private float _speedMultiplier = 1.0f;

        private RectTransform RcTransform => (RectTransform)transform;
        private RectTransform ContentRcTransform => (RectTransform)_content.transform;

        public float SpeedMultiplier
        {
            get { return _speedMultiplier; }
            set { _speedMultiplier = value; }
        }

        public int ItemsCount => _items.Count;

        public int VisibleItemsCount => _visibleItemsCount;

        public List<SlotItem> Items => _items;

        public void InitLine()
        {
            _items.AddRange(_content.GetComponentsInChildren<SlotItem>());

            _centerItemIndex = _items.IndexOf(_centerItem);

            AutoLayoutItems();
        }

        public void StartRotation()
        {
            _isRotateInProgress = true;
            _isRotateFinished = false;
             _rotateRoutine = StartCoroutine(RotateRoutine());
        }

        public IEnumerator StopRotationRoutine()
        {
            if (_rotateRoutine != null)
            {
                _isRotateInProgress = false;
                yield return new WaitUntil(() => _isRotateFinished);
            }
        }

        private IEnumerator RotateRoutine()
        {
            Vector2 totalOffset = new(0, _itemSize.y + _itemsSpacing);

            while (_isRotateInProgress)
            {
                Vector2 zeroItemPos = _items[0].RectTransform.anchoredPosition;

                float timer = 0f;
                float maxTime = 1f;

                List<Vector2> itemsPositions = _items
                    .Select(i => i.RectTransform.anchoredPosition)
                    .ToList();

                List<Vector2> itemsNewPositions = itemsPositions
                    .Select(i => i - totalOffset)
                    .ToList();

                while (timer <= maxTime)
                {
                    timer += Time.deltaTime * _speedMultiplier;
                    
                    for (int i = 0; i < _items.Count; ++i)
                    {
                        _items[i].RectTransform.anchoredPosition =
                            Vector2.Lerp(itemsPositions[i], itemsNewPositions[i], timer);
                    }

                    yield return null;
                }

                SlotItem lastItem = _items.Last();
                lastItem.RectTransform.anchoredPosition = zeroItemPos;

                _items.Remove(lastItem);
                _items.Insert(0, lastItem);
            }

            _isRotateFinished = true;
        }

        private void AutoLayoutItems()
        {
            float height = _visibleItemsCount * _itemSize.y + _itemsSpacing * (_visibleItemsCount + 1);

            RcTransform.sizeDelta = new Vector2(_lineWidth, height);

            Debug.Log(_items[0].RectTransform.anchoredPosition);
            Debug.Log(ContentRcTransform.anchoredPosition);

            _items[_centerItemIndex].RectTransform.anchoredPosition = new(0, 0);

            for (int i = 0; i < _centerItemIndex; ++i)
            {
                Vector2 newAnchPos = new(0, 
                    (_centerItemIndex - i) * (_itemSize.y + _itemsSpacing));

                _items[i].RectTransform.anchoredPosition = newAnchPos;
            }

            for (int i = _centerItemIndex + 1; i < _items.Count; ++i)
            {
                Vector2 newAnchPos = new(0,
                    (i - _centerItemIndex) * -1 * (_itemSize.y + _itemsSpacing));

                _items[i].RectTransform.anchoredPosition = newAnchPos;
            }
        }
    }
}
