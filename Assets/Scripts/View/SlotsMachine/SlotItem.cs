﻿using UnityEngine;

namespace Scripts.View.SlotsMachine
{
    internal class SlotItem : MonoBehaviour
    {
        [SerializeField] private int _id;
        public RectTransform RectTransform => (RectTransform)transform;
        public int Id => _id;
    }
}
