﻿
namespace Scripts.View
{
    internal interface IScreen
    {
        public void InitScreen();

        public void ShowScreen();

        public void HideScreen();
    }
}
