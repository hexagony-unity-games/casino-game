﻿using UnityEngine;

namespace Scripts.View.LoadingScreen
{
    internal class LoadingCircle : MonoBehaviour
    {
        private static readonly int START = Animator.StringToHash("start");
        private static readonly int STOP = Animator.StringToHash("stop");
        private static readonly int SPEED = Animator.StringToHash("speedMultiplier");

        [SerializeField] private Animator _animator;

        [SerializeField] private float _speedMultiplier = 1.0f;

        private void Awake()
        {
            _animator.SetFloat(SPEED, _speedMultiplier);
        }

        public void StartAnimation()
        {
            _animator.SetTrigger(START);
        }

        public void StopAnimation()
        {
            _animator.SetTrigger(STOP);
        }
    }
}
