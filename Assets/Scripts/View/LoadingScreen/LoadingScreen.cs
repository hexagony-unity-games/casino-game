﻿using Scripts.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts.View.LoadingScreen
{
    internal class LoadingScreen : MonoBehaviour, IScreen
    {
        [SerializeField] private GameObject _rootScreenObject;
        [SerializeField] private LoadingCircle _loadingCircle;
        [Header("Settings")]
        [SerializeField] private bool _showOnAwake = false;

        public void InitScreen()
        {
            if (_showOnAwake)
            {
                ShowScreen();
            }
        }

        public void ShowScreen()
        {
            _rootScreenObject.SetActive(true);
            _loadingCircle.StartAnimation();
        }

        public void HideScreen()
        {
            _rootScreenObject.SetActive(false);
            _loadingCircle.StopAnimation();
        } 
    }
}
