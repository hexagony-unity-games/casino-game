﻿using Scripts.Model;
using Scripts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Scripts.View.UserProfileScreen
{
    internal class UserProfileView : MonoBehaviour
    {
        [Header("Name")]
        [SerializeField] private string _nameViewTemplate = "{0}";
        [SerializeField] private TMP_Text _nameView;
        [Header("Coins")]
        [SerializeField] private string _coinsViewTemplate = "{0} $";
        [SerializeField] private TMP_Text _coinsView;
        [Header("Win percent")]
        [SerializeField] private string _winPercentViewTemplate = "{0:f2}%";
        [SerializeField] private TMP_Text _winPercentView;
        [Header("AVG win amount")]
        [SerializeField] private string _avgWinAmountViewTemplate = "{0:f2} $";
        [SerializeField] private TMP_Text _avgWinAmountView;

        public void UpdateView(UserProfile user)
        {
            _nameView.text = string.Format(_nameViewTemplate, user.Name);
            _coinsView.text = string.Format(_coinsViewTemplate, user.Coins);
            _winPercentView.text = string.Format(_winPercentViewTemplate, user.WinningPercent * 100);
            _avgWinAmountView.text = string.Format(_avgWinAmountViewTemplate, user.AvgWinningScore);
        }
    }
}
