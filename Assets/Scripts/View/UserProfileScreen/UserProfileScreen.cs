﻿using Scripts.Model;
using Scripts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.View.UserProfileScreen
{
    internal class UserProfileScreen : MonoBehaviour, IScreen
    {
        [SerializeField] private GameObject _screenRoot;
        [SerializeField] private UserProfileScreenVM _viewModel;
        [SerializeField] private UserProfileView _userProfileView;
        [SerializeField] private Button _closeBtn;

        public void InitScreen()
        {
            _closeBtn.onClick.AddListener(_viewModel.CloseAction);
            HideScreen();
        }

        public void ShowScreen()
        {
            _screenRoot.SetActive(true);
        }

        public void HideScreen()
        {
            _screenRoot.SetActive(false);
        }

        public void UpdateUserProfileInfo(UserProfile user)
        {
            _userProfileView.UpdateView(user);
        }
    }
}
