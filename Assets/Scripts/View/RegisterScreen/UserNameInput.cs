﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Scripts.View.RegisterScreen
{
    internal class UserNameInput : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _inputField;
        [SerializeField] private string _blackList = "$;:@!_-./\\,&*%?<>[]{}()|+=`~#№^";
        [SerializeField] private UnityEvent<string> _onValueChanged;

        public string UserName { 
            get
            {
                return _inputField.text;
            } 
        }

        public UnityEvent<string> OnValueChanged => _onValueChanged;

        private void Awake()
        {
            _inputField.onValueChanged.AddListener(OnValueChangedListener);
        }

        public void OnValueChangedListener(string value)
        {
            List<string> blackList = _blackList
                .ToList()
                .Select(c => c.ToString())
                .ToList();

            blackList.ForEach(c => value = value.Replace(c, ""));

            _inputField.text = value;

            _onValueChanged?.Invoke(value);
        }
    }
}
