﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Scripts.View.RegisterScreen
{
    [RequireComponent(typeof(Button))]
    internal class RegisterUserBtn : MonoBehaviour
    {
        [SerializeField] private UserNameInput _userNameInput;
        [SerializeField] private Button _button;

        public Button.ButtonClickedEvent OnClicked => _button.onClick;

        private void Awake()
        {
            _userNameInput.OnValueChanged.AddListener(OnValueChangedListener);

            if (_userNameInput.UserName.Length == 0)
            {
                _button.interactable = false;
            }
        }

        private void OnValueChangedListener(string value)
        {
            _button.interactable = value.Length > 0;
        }
    }
}
