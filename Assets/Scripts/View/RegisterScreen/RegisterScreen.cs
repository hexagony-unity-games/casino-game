﻿using Scripts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts.View.RegisterScreen
{
    internal class RegisterScreen : MonoBehaviour, IScreen
    {
        [SerializeField] private GameObject _screenRoot;
        [SerializeField] private RegisterScreenViewModel _registerScreenVm;
        [Space(10)]
        [SerializeField] private UserNameInput _usernameInput;
        [SerializeField] private RegisterUserBtn _registerUserBtn;

        public void InitScreen()
        {
            _registerUserBtn.OnClicked.AddListener(OnEnterPressed);
            HideScreen();
        }

        public void ShowScreen()
        {
            _screenRoot.SetActive(true);
        }

        public void HideScreen()
        {
            _screenRoot.SetActive(false);
        }

        public void OnEnterPressed()
        {
            _registerScreenVm.RegisterNewUser(_usernameInput.UserName);
        }
    }
}
