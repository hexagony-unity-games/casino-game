﻿using Scripts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.View.GameScreen
{
    internal class BetValueChanger : MonoBehaviour
    {
        [SerializeField] private int _initialBetValue = 100;
        [SerializeField] private int _minBet = 50;
        [SerializeField] private int _betDelta = 10;
        [SerializeField] private string _betViewMask = "BET: {0} $";
        [SerializeField] private TMP_Text _betView;
        [SerializeField] private Button _betIncreaseBtn;
        [SerializeField] private Button _betDecreaseBtn;

        private int _curBet;
        private int _maxBetValue;

        public int CurBetValue => _curBet;

        private void Awake()
        {
            _betDecreaseBtn.onClick.AddListener(BetDecrease);
            _betIncreaseBtn.onClick.AddListener(BetIncrease);
            ResetBet();
        }

        public void UpdateMaxBetValue(int maxBetValue)
        {
            _maxBetValue = maxBetValue;
        }

        public void SetInteractible(bool isInteractible)
        {
            _betDecreaseBtn.interactable = isInteractible;
            _betIncreaseBtn.interactable = isInteractible;
        }

        public void ResetBet()
        {
            _curBet = _initialBetValue;
            UpdateView();
        }

        private void BetIncrease()
        {
            _curBet += _betDelta;

            _curBet = _curBet > _maxBetValue ? _maxBetValue : _curBet;

            UpdateView();
        }

        private void BetDecrease()
        {
            _curBet -= _betDelta;

            _curBet = _curBet < _minBet ? _minBet : _curBet;

            UpdateView();
        }

        private void UpdateView()
        {
            _betView.text = String.Format(_betViewMask, _curBet);
        }
    }
}
