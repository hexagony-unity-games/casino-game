﻿using Scripts.Model;
using Scripts.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Scripts.View.GameScreen
{
    internal class UserBalanceView : MonoBehaviour
    {
        [SerializeField] private string _viewTemplate = "{0} $";
        [SerializeField] private TMP_Text _view;

        public void UpdateView(UserProfile user)
        {
            int curValue = user.Coins;
            _view.text = String.Format(_viewTemplate, curValue);
        }
    }
}
