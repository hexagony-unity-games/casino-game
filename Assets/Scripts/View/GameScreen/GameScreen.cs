﻿using Scripts.ViewModel;
using Scripts.View.SlotsMachine;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.View.GameScreen
{
    internal class GameScreen : MonoBehaviour, IScreen
    {
        [SerializeField] private GameObject _screenRoot;
        [SerializeField] private GameScreenViewModel _viewModel;

        [SerializeField] private SlotsMachine.SlotsMachine _slotsMachine;
        [SerializeField] private BetValueChanger _betValueChanger;
        [SerializeField] private UserBalanceView _userBalanceView;
        [SerializeField] private Button _startGameBtn;
        [SerializeField] private Button _goToProfileBtn;
        [SerializeField] private Button _exitBtn;

        public void InitScreen()
        {
            _startGameBtn.onClick.AddListener(StartGame);
            _goToProfileBtn.onClick.AddListener(_viewModel.GoToProfileAction);
            _exitBtn.onClick.AddListener(ExitGame);

            _slotsMachine.OnRotationEnded.AddListener(OnRotationEndedListener);
            _slotsMachine.InitMachine();

            HideScreen();
        }

        public void ShowScreen()
        {
            _screenRoot.SetActive(true);
            _userBalanceView.UpdateView(_viewModel.GetUserProfile);
        }

        public void HideScreen()
        {
            _screenRoot.SetActive(false);
        }

        public void StartGame()
        {
            _startGameBtn.interactable = false;
            _betValueChanger.SetInteractible(false);
            _userBalanceView.UpdateView(_viewModel.GetUserProfile);
            _slotsMachine.StartRotation();
        }

        private void OnRotationEndedListener(SlotItem[,] items)
        {
            _startGameBtn.interactable = true;
            _betValueChanger.SetInteractible(true);
            _betValueChanger.UpdateMaxBetValue(_viewModel.GetUserProfile.Coins);

            _viewModel.OnGameEnded(items, _betValueChanger.CurBetValue);

            _userBalanceView.UpdateView(_viewModel.GetUserProfile);
            _betValueChanger.ResetBet();
        }

        private void ExitGame()
        {
            Application.Quit();
        }
    }
}
