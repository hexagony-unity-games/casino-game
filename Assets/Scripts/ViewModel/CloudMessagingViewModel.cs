﻿using Scripts.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.ViewModel
{
    internal class CloudMessagingViewModel : AViewModel
    {
        private FBCloudMessagingApi _api;

        public override void Init()
        {
            _api = new FBCloudMessagingApi();
            _api.InitAfterFirebaseInitAsync();
        }

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            throw new NotImplementedException();
        }
    }
}
