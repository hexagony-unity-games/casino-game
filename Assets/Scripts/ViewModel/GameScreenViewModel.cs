﻿using Scripts.Model;
using Scripts.View.GameScreen;
using Scripts.View.SlotsMachine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Scripts.ViewModel
{
    internal class GameScreenViewModel : AViewModel
    {
        [SerializeField] private GameScreen _gameScreen;
        [SerializeField] private float _baseKoeff = 4f;
        [SerializeField] private float _koeffRndOffset = 0.5f;

        private UserProfilesViewModel _userProfiles;
        private UserProfileScreenVM _userProfileScreenVm;

        public UserProfile GetUserProfile => _userProfiles.GetCurrentUser();

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            _userProfiles = ExtractDependency<UserProfilesViewModel>(dependencies);
            _userProfileScreenVm = ExtractDependency<UserProfileScreenVM>(dependencies);
        }

        public override void Init()
        {
            HideScreen();
            _gameScreen.InitScreen();
        }

        public void HideScreen()
        {
            _gameScreen.HideScreen();
        }

        public void ShowScreen()
        {
            _gameScreen.ShowScreen();
        }

        public void GoToProfileAction()
        {
            _gameScreen.HideScreen();
            _userProfileScreenVm.ShowScreen();
        }

        public void OnGameEnded(SlotItem[,] items, int betValue)
        {
            int height = items.GetLength(0);
            int wigth = items.GetLength(1);

            List<List<SlotItem>> matchedPatterns = new();

            for (int y = 0; y < height; ++y)
            {
                int curId = items[y, 0].Id;
                var curMatch = new List<SlotItem>(new[] { items[y, 0] });

                for (int x = 1; x < wigth; ++x)
                {
                    if (curId == items[y, x].Id)
                    {
                        curMatch.Add(items[y, x]);
                    }
                }

                if (curMatch.Count == wigth)
                {
                    matchedPatterns.Add(curMatch);
                }
            }

            bool isWinGame = false;
            int winCoinsCount;

            if (matchedPatterns.Count > 0)
            {
                float winKoeff = UnityEngine.Random.Range(_baseKoeff, _baseKoeff + _koeffRndOffset);

                winCoinsCount = (int)Mathf.Round(
                    Mathf.Pow(winKoeff, matchedPatterns.Count) * betValue);

                isWinGame = true;
            }
            else
            {
                winCoinsCount = -1 * betValue;
            }

            var gameRoundResult = new GameRoundResult(isWinGame, winCoinsCount);

            _userProfiles.UpdateCurUserBalance(winCoinsCount);
            _userProfiles.SaveCurUserGameResult(gameRoundResult);
            Debug.Log(gameRoundResult);
        }
    }
}
