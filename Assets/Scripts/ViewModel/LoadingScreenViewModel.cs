﻿using Scripts.Model;
using Scripts.View;
using Scripts.View.LoadingScreen;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts.ViewModel
{
    internal class LoadingScreenViewModel : AViewModel
    {
        [SerializeField] private LoadingScreen _loadingScreen;

        private RemoteConfigViewModel _remoteConfigVm;
        private WebViewerViewModel _webViewerVm;
        private GameScreenViewModel _gameScreenVm;
        private RegisterScreenViewModel _registerScreenVm;
        private AppSettingsViewModel _appSettingsVm;

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            _remoteConfigVm = ExtractDependency<RemoteConfigViewModel>(dependencies);
            _webViewerVm = ExtractDependency<WebViewerViewModel>(dependencies);
            _gameScreenVm = ExtractDependency<GameScreenViewModel>(dependencies);
            _registerScreenVm = ExtractDependency<RegisterScreenViewModel>(dependencies);
            _appSettingsVm = ExtractDependency<AppSettingsViewModel>(dependencies);
        }

        public override void Init()
        {
            _loadingScreen.InitScreen();
            GetRemoteConfig();
        }

        private async void GetRemoteConfig()
        {
            RemoteConfig config = await _remoteConfigVm.GetRemoteConfig();

            if (Debug.isDebugBuild)
            {
                await Task.Delay(5000);
            }
            
            if (config.WebViewEnabled)
            {
                _webViewerVm.ShowWebView();
                _webViewerVm.LoadURL(config.WebViewUrl);
            } 
            else
            {
                if (_appSettingsVm.IsAnyUserRegistered)
                {
                    _gameScreenVm.ShowScreen();
                }
                else
                {
                    _registerScreenVm.ShowScreen();
                }
            }

            _loadingScreen.HideScreen();
        }
    }
}
