﻿using Scripts.Repository.Interface;
using Scripts.Repository;
using UnityEngine;
using Scripts.Model;
using UnityEngine.Events;
using Firebase;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Scripts.ViewModel
{
    internal class RemoteConfigViewModel : AViewModel
    {
        [SerializeField] private UnityEvent<RemoteConfig> _onConfigLoaded;
        private IRemoteConfigRepository _remoteConfigRepository;

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            throw new System.NotImplementedException();
        }

        public override void Init()
        {
            _remoteConfigRepository = new RemoteConfigRepository();
        }

        public async Task<RemoteConfig> GetRemoteConfig()
        {
            var remoteConfig = await _remoteConfigRepository.GetRemoteConfigAsync();
            _onConfigLoaded?.Invoke(remoteConfig);
            return remoteConfig;
        }
    }
}
