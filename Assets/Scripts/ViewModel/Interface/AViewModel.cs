﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts.ViewModel
{
    internal abstract class AViewModel : MonoBehaviour
    {
        protected T ExtractDependency<T>(Dictionary<string, AViewModel> dependencies) where T : AViewModel
        {
            return (T)dependencies[typeof(T).Name];
        }

        public abstract void Init();

        public abstract void SetDependencies(Dictionary<string, AViewModel> dependencies);
    }
}
