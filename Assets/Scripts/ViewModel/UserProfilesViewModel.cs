﻿using Scripts.DataSource;
using Scripts.Model;
using Scripts.Repository;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Scripts.ViewModel
{
    internal class UserProfilesViewModel : AViewModel
    {
        [SerializeField] private string _storageFileName;

        [Header("Options")]
        [SerializeField] private int _userInitialCoinsCount = 1000;
        [Header("Debug tools")]
        [SerializeField] private bool _resetData = false;
        [SerializeField] private bool _overrideData = false;
        [SerializeField] private int _userCoinsCountOverride = 100;

        private string _filePath;

        private UserProfileRepository _repository;

        private AppSettingsViewModel _appSettings;

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            _appSettings = ExtractDependency<AppSettingsViewModel>(dependencies);
        }

        public override void Init()
        {
            _filePath = $"{Application.persistentDataPath}\\{_storageFileName}.bin";

            if (_resetData)
            {
                File.Delete(_filePath);
            }

            var usersFs = new UserProfileFileSource(_filePath);
            _repository = new UserProfileRepository(usersFs);

        }

        public void RegisterNewUser(string username, bool setAsCurrent = true)
        {
            int coinsCount = _overrideData ? _userCoinsCountOverride : _userInitialCoinsCount;

            var newUser = new UserProfile(username, coinsCount);

            _repository.SaveProfile(newUser);

            if (setAsCurrent || !_appSettings.IsAnyUserRegistered || _resetData)
            {
                _appSettings.CurUserId = newUser.Id;
                _appSettings.IsAnyUserRegistered = true;
            }
        }

        public void UpdateBalance(int userId, int balanceDifference)
        {
            UserProfile profile = _repository.GetProfile(userId);
            profile.Coins += balanceDifference;
            _repository.SaveProfile(profile);
        }

        public void UpdateCurUserBalance(int balanceDifference)
        {
            UserProfile profile = _repository.GetProfile(_appSettings.CurUserId);
            profile.Coins += balanceDifference;
            _repository.SaveProfile(profile);
        }

        public UserProfile GetUser(int userId)
        {
            return _repository.GetProfile(userId);
        }

        public UserProfile GetCurrentUser()
        {
            return _repository.GetProfile(_appSettings.CurUserId);
        }

        public void SaveGameResult(int userId, GameRoundResult result)
        {
            _repository.SaveGameResult(userId, result);
        }

        public void SaveCurUserGameResult(GameRoundResult result)
        {
            _repository.SaveGameResult(_appSettings.CurUserId, result);
        }
    }
}
