﻿using Scripts.View;
using Scripts.View.RegisterScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts.ViewModel
{
    internal class RegisterScreenViewModel : AViewModel
    {
        [SerializeField] private RegisterScreen _registerScreen;
        

        private UserProfilesViewModel _usersVm;
        private GameScreenViewModel _gameScreenVm;

        public override void Init()
        {
            HideScreen();
            _registerScreen.InitScreen();
        }

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            _usersVm = ExtractDependency<UserProfilesViewModel>(dependencies);
            _gameScreenVm = ExtractDependency<GameScreenViewModel>(dependencies);
        }

        public void RegisterNewUser(string username)
        {
            _usersVm.RegisterNewUser(username);
            _gameScreenVm.ShowScreen();
            _registerScreen.HideScreen();
        }

        public void ShowScreen()
        {
            _registerScreen.ShowScreen();
        }

        public void HideScreen()
        {
            _registerScreen.HideScreen();
        }
    }
}
