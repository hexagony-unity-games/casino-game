﻿using Scripts.DataSource;
using Scripts.Repository;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Scripts.ViewModel
{
    internal class AppSettingsViewModel : AViewModel
    {
        [SerializeField] private string _storageFileName;
        [Header("Display")]
        [SerializeField] private bool _useDisplayRefreshRate = true;
        [SerializeField] private int _refreshRate = 60;
        [Header("Debug tools")]
        [SerializeField] private bool _resetData = false;
        [SerializeField] private bool _overrideValues = false;
        [SerializeField] private int _curUserIdOverride = 1;
        [SerializeField] private bool _isAnyUserRegisteredOverride = false;

        private string _filePath;

        private AppSettingsRepository _repository;

        public bool IsAnyUserRegistered { 
            get => _repository.IsAnyUserRegistered; 
            set => _repository.IsAnyUserRegistered = value; 
        }

        public int CurUserId
        {
            get => _repository.CurrentUserId;
            set => _repository.CurrentUserId = value;
        }

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            throw new System.NotImplementedException();
        }

        public override void Init()
        {
            _filePath = $"{Application.persistentDataPath}\\{_storageFileName}.bin";

            if (_resetData)
            {
                File.Delete(_filePath);
            }

            var settingsFs = new AppSettingsFileSource(_filePath);
            _repository = new AppSettingsRepository(settingsFs);

            if (_overrideValues && Debug.isDebugBuild)
            {
                _repository.IsAnyUserRegistered = _isAnyUserRegisteredOverride;
                _repository.CurrentUserId = _curUserIdOverride;
            }

            QualitySettings.vSyncCount = 0;
            if (_useDisplayRefreshRate)
            {
                Application.targetFrameRate = Screen.currentResolution.refreshRate;
            } 
            else
            {
                Application.targetFrameRate = _refreshRate;
            }
        }
    }
}
