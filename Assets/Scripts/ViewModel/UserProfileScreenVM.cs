﻿using Scripts.Model;
using Scripts.View.UserProfileScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.ViewModel
{
    internal class UserProfileScreenVM : AViewModel
    {
        [SerializeField] private UserProfileScreen _profileScreen;

        private UserProfilesViewModel _userProfilesVm;
        private GameScreenViewModel _gameScreenVm;

        public override void Init()
        {
            _profileScreen.InitScreen();
        }

        public override void SetDependencies(Dictionary<string, AViewModel> dependencies)
        {
            _userProfilesVm = ExtractDependency<UserProfilesViewModel>(dependencies);
            _gameScreenVm = ExtractDependency<GameScreenViewModel>(dependencies);
        }

        public void ShowScreen()
        {
            UserProfile user = _userProfilesVm.GetCurrentUser();

            _profileScreen.ShowScreen();
            _profileScreen.UpdateUserProfileInfo(user);
        }

        public void HideScreen()
        {
            _profileScreen.HideScreen();
        }

        public void CloseAction()
        {
            HideScreen();
            _gameScreenVm.ShowScreen();
        }
    }
}
