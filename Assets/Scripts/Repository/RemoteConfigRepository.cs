﻿using Scripts.Api;
using Scripts.Model;
using Scripts.Repository.Interface;
using System.Threading.Tasks;

namespace Scripts.Repository
{
    internal class RemoteConfigRepository : IRemoteConfigRepository
    {
        private FBRemoteConfigApi _configApi;

        private RemoteConfig _lastConfig;

        private bool _isFirstConfigAccess = true;

        public RemoteConfigRepository()
        {
            _configApi = new FBRemoteConfigApi();
        }

        public async Task<RemoteConfig> GetRemoteConfigAsync(bool needToUpdateCache = false)
        {
            if (_lastConfig == null || needToUpdateCache)
            {
                if (_isFirstConfigAccess)
                {
                    await _configApi.SetDefaulConfigValuesAsync();
                    _isFirstConfigAccess = false;
                }

                _lastConfig = await _configApi.GetRemoteConfigAsync();
            }

            return _lastConfig;
        }
    }
}
