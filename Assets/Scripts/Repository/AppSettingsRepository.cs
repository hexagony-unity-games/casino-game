﻿using Scripts.DataSource;
using Scripts.Model;
using Scripts.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.Repository
{
    internal class AppSettingsRepository : IAppSettingsRepository
    {
        private AppSettingsFileSource _fs;

        private AppSettings _cusSettings;

        public int CurrentUserId {
            get => _cusSettings.CurrentUserId;
            set
            {
                _cusSettings.CurrentUserId = value;
                _fs.WriteSettings(_cusSettings);
            }
        }
        public bool IsAnyUserRegistered {
            get => _cusSettings.IsAnyUserRegistered;
            set
            {
                _cusSettings.IsAnyUserRegistered = value;
                _fs.WriteSettings(_cusSettings);
            }
        }

        public AppSettingsRepository(AppSettingsFileSource fs)
        {
            _fs = fs;
            _cusSettings = _fs.ReadSettings();
        }
    }
}
