﻿using Scripts.DataSource;
using Scripts.Model;
using Scripts.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scripts.Repository
{
    internal class UserProfileRepository : IUserProfileRepository
    {
        private IUserProfileSource _profilesSource;

        private List<UserProfile> _profiles;

        public UserProfileRepository(IUserProfileSource profilesSource)
        {
            _profilesSource = profilesSource;

            _profiles = _profilesSource.ReadAllUsers();
        }

        public UserProfile GetProfile(int userId)
        {
            UserProfile profile = _profiles.Find(u => u.Id == userId);

            if (profile is null)
            {
                throw new ArgumentException(
                    "User with specified id does not exists",
                    nameof(userId));
            } 

            return profile;
        }

        public void SaveProfile(UserProfile profile)
        {
            UserProfile oldProfile = _profiles.Find(p => p.Id == profile.Id);

            if (oldProfile is not null)
            {
                _profiles.Remove(oldProfile);
            }

            _profiles.Add(profile);

            _profilesSource.WriteAllUsers(_profiles);
        }

        public void SaveGameResult(int userId, GameRoundResult result)
        {
            UserProfile profile = GetProfile(userId);

            profile.AddPlayedGame(result);

            SaveProfile(profile);
        }
    }
}
