﻿using Scripts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.Repository.Interface
{
    internal interface IUserProfileRepository
    {
        public UserProfile GetProfile(int userId);
        public void SaveProfile(UserProfile profile);
        public void SaveGameResult(int userId, GameRoundResult result);
    }
}
