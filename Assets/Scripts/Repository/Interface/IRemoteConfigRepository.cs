﻿using Scripts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.Repository.Interface
{
    internal interface IRemoteConfigRepository
    {
        public Task<RemoteConfig> GetRemoteConfigAsync(bool needToUpdateCache = false);
    }
}
