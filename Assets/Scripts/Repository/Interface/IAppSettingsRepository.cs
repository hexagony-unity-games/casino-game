﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.Repository.Interface
{
    internal interface IAppSettingsRepository
    {
        public int CurrentUserId { get; set; }

        public bool IsAnyUserRegistered { get; set; }
    }
}
