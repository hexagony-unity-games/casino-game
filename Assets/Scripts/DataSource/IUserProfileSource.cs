﻿using Scripts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.DataSource
{
    internal interface IUserProfileSource
    {
        public List<UserProfile> ReadAllUsers();
        public void WriteAllUsers(List<UserProfile> users);
    }
}
