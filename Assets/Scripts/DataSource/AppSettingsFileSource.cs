﻿using Scripts.Model;
using System;
using System.IO;
using UnityEngine;

namespace Scripts.DataSource
{
    internal class AppSettingsFileSource
    {
        private string _filePath = "";

        public AppSettingsFileSource(string filePath) 
        {
            _filePath = filePath;
        }

        public AppSettings ReadSettings()
        {
            var settings = new AppSettings();

            try
            {
                Stream s = File.OpenRead(_filePath);
                settings.Deserialize(s);

                s.Close();
            } 
            catch (Exception e) 
            {
                Debug.LogError($"Unable to read file: {_filePath} :: {e}");
            }

            return settings;
        }

        public void WriteSettings(AppSettings settings)
        {
            using (Stream s = File.OpenWrite(_filePath))
            {
                settings.Serialize(s);
            }
        }
    }
}
