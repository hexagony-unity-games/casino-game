﻿using Scripts.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Scripts.DataSource
{
    internal class UserProfileFileSource : IUserProfileSource
    {
        private string _filePath = "";

        public UserProfileFileSource(string filePath)
        {
            _filePath = filePath;
        }

        public List<UserProfile> ReadAllUsers()
        {
            List<UserProfile> users = new();

            try
            {
                Stream s = File.OpenRead(_filePath);

                using (var br = new BinaryReader(s))
                {
                    int usersCount = br.ReadInt32();

                    for (int i = 0; i < usersCount; i++)
                    {
                        UserProfile profile = new();
                        profile.Deserialize(s);
                        users.Add(profile);
                    }
                }

                s.Close();
            } 
            catch (Exception e)
            {
                Debug.LogError($"Unable to read file: {_filePath} :: {e}");
            }

            return users;
        }

        public void WriteAllUsers(List<UserProfile> users)
        {
            if (users.Count == 0)
            {
                return;
            }

            using (Stream s = File.OpenWrite(_filePath))
            {
                using (var bw = new BinaryWriter(s, Encoding.Default, true))
                {
                    bw.Write(users.Count);
                }

                users.ForEach(u => u.Serialize(s));
            }
        }
    }
}
