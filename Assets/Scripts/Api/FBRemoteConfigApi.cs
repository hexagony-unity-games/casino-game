﻿using Firebase.RemoteConfig;
using Scripts.Api.Const;
using Scripts.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Scripts.Api
{
    internal class FBRemoteConfigApi
    {
        private const bool DEFAULT_WEB_VIEW_ENABLING = false;
        private const string DEFAULT_WEB_VIEW_URL = "https://google.com/";

        public async Task SetDefaulConfigValuesAsync()
        {
            Dictionary<string, object> appDefaults = new()
            {
                {RemoteConfigKey.WebViewEnabled, DEFAULT_WEB_VIEW_ENABLING},
                {RemoteConfigKey.WebViewUrl, DEFAULT_WEB_VIEW_URL},
            };

            FirebaseRemoteConfig rc = await FirebaseApi.RCInstanceAsync();

            await rc.SetDefaultsAsync(appDefaults);
        }

        public async Task<RemoteConfig> GetRemoteConfigAsync()
        {

            FirebaseRemoteConfig rc = await FirebaseApi.RCInstanceAsync();

            try
            {

                Task fetchTask = rc.FetchAsync(TimeSpan.Zero);

                await fetchTask;

                if (!fetchTask.IsCompleted)
                {
                    Debug.LogError($"Remote config fetch failed! Reason:" +
                        $"{rc.Info.LastFetchFailureReason}");
                }
                else
                {
                    Debug.Log($"Remote config fetch succeded! Details: " +
                        $"{rc.Info.LastFetchStatus}");
                    if (rc.Info.LastFetchStatus == LastFetchStatus.Success)
                    {
                        await rc.ActivateAsync();
                    }
                }
            }
            catch (Exception e)
            {
                await SetDefaulConfigValuesAsync();

                Debug.LogError($"Remote config fetch failed! Reasons: " +
                    $"{rc.Info.LastFetchFailureReason} {e.Message}");
            }

            return UnpackConfig();
        }

        private RemoteConfig UnpackConfig()
        {
            FirebaseRemoteConfig fbInstance = FirebaseRemoteConfig.DefaultInstance;

            ConfigValue webViewEnabled = fbInstance.GetValue(RemoteConfigKey.WebViewEnabled);
            ConfigValue webViewUrl = fbInstance.GetValue(RemoteConfigKey.WebViewUrl);

            var config = new RemoteConfig(
                webViewEnabled.BooleanValue,
                webViewUrl.StringValue
                );

            Debug.Log($"Current config data: {config}");

            return config;
        }
    }
}
