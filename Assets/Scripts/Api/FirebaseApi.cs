﻿using Firebase;
using JetBrains.Annotations;
using UnityEngine;
using Firebase.RemoteConfig;
using System.Threading.Tasks;
using Firebase.Messaging;

namespace Scripts.Api
{
    internal static class FirebaseApi
    {

        private static bool s_isInitStarted = false;

        private static Task s_initTask = null;

        public static bool IsAvailable { get; private set; } = false;

        public static bool IsInitFinished { get; private set; } = false;

        [CanBeNull]
        public static FirebaseApp App { get; private set; } = null;

        [CanBeNull]
        public static FirebaseRemoteConfig RCInstance { get; private set; } = null;

        public static async Task<FirebaseRemoteConfig> RCInstanceAsync()
        {
            await ConfigureFirebaseApiAsync();
            return RCInstance;
        }

        public static async Task<FirebaseApp> AppAsync()
        {
            await ConfigureFirebaseApiAsync();
            return App;
        }

        public static async Task ConfigureFirebaseApiAsync()
        {
            if (s_isInitStarted)
            {
                await s_initTask;
                return;
            }

            s_isInitStarted = true;

            s_initTask = InternalConfigFBAsync();
            await s_initTask;

            IsInitFinished = true;
        }

        private static async Task InternalConfigFBAsync()
        {
            DependencyStatus dependencyStatus = await FirebaseApp.CheckAndFixDependenciesAsync();

            if (dependencyStatus == DependencyStatus.Available)
            {
                App = FirebaseApp.DefaultInstance;
                RCInstance = FirebaseRemoteConfig.DefaultInstance;
                IsAvailable = true;
                Debug.Log(string.Format(
                  "All Firebase dependencies were resolved: {0}", dependencyStatus));
            }
            else
            {
                Debug.LogError(string.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            }
        }
    }
}
