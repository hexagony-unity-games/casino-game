﻿using Firebase.Messaging;
using UnityEngine.Events;
using UnityEngine;

namespace Scripts.Api
{
    internal class FBCloudMessagingApi
    {
        public UnityEvent<MessageReceivedEventArgs> OnMessageReceived { get; private set; } = new();
        public UnityEvent<TokenReceivedEventArgs> OnTokenReceived { get; private set; } = new();

        public FBCloudMessagingApi() 
        {
            FirebaseMessaging.TokenRegistrationOnInitEnabled = true;
        }

        public async void InitAfterFirebaseInitAsync()
        {
            await FirebaseApi.AppAsync();
            FirebaseMessaging.TokenReceived += OnTokenReceivedListener;
            FirebaseMessaging.MessageReceived += OnMessageReceivedListener;
            Debug.Log($"Init cloud messaging afret FB");
            Debug.Log($"Awaiting token...");
            string token = await FirebaseMessaging.GetTokenAsync();
            Debug.Log($"FCM token: {token}");
        }

        private void OnMessageReceivedListener(object sender, MessageReceivedEventArgs e)
        {
            Debug.Log($"Received a new message: {e.Message.MessageId} from: {e.Message.From}");
            OnMessageReceived.Invoke(e);
        }

        private void OnTokenReceivedListener(object sender, TokenReceivedEventArgs e)
        {
            Debug.Log($"Received Registration Token: {e.Token}");
            OnTokenReceived.Invoke(e);
        }
    }
}
