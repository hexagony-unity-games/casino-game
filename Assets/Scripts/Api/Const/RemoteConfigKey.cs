﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripts.Api.Const
{
    internal class RemoteConfigKey
    {
        public static string WebViewEnabled = "web_view_enabled";
        public static string WebViewUrl = "web_view_url";
    }
}
